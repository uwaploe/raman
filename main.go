// Raman runs a Raman spectroscopy data acquisition sequence. All of the subsystems
// must be powered-on before running this application.
package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/beamexp"
	iccd "bitbucket.org/uwaploe/iccdctl"
	"bitbucket.org/uwaploe/imagesvc"
	"bitbucket.org/uwaploe/tbctl/pkg/tb"
	"bitbucket.org/uwaploe/viron"
	"github.com/briandowns/spinner"
	"github.com/tarm/serial"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

const Usage = `Usage: raman paramfile [count]

Run a Raman spectroscopy data acquisition sequence and collect
COUNT images (default 1).
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debugMode = flag.Bool("debug", false,
		"If true enable debugging output")
	intensBaud  int    = 115200
	intensAddr  int    = 7
	intensDev   string = "/dev/ttyS1"
	expDev      string = "/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A907FQ0N-if00-port0"
	expBaud     int    = 9600
	svcAddr     string = "localhost:10130"
	haveEcho    bool   = true
	laserAddr   string = "10.40.7.11:23"
	laserPword  string
	laserWarmup time.Duration = time.Minute * 5
	tbAddr      string        = "/run/tb.sock"
	outDir      string        = "."
)

// Messages for the Timing Board server
var FpgaEnable tb.Message = tb.Message{Cmd: "enable"}
var FpgaDisable tb.Message = tb.Message{Cmd: "disable"}
var FpgaLoad tb.Message = tb.Message{Cmd: "load"}

type system struct {
	intensifier *iccd.Intensifier
	laserConn   net.Conn
	laserDev    *viron.Device
	iClient     *imagesvc.Client
	beDev       *beamexp.Device
}

func sendMessages(sock string, msgs ...tb.Message) error {
	conn, err := net.Dial("unix", sock)
	if err != nil {
		return fmt.Errorf("Server connect failed: %v", err)
	}
	defer conn.Close()

	enc := json.NewEncoder(conn)
	for _, msg := range msgs {
		err := enc.Encode(msg)
		if err != nil {
			return err
		}
	}
	return nil
}

func vironSetup(conn net.Conn, dev *viron.Device) error {
	cmds := []viron.CommandLine{
		viron.NewCommandLine("TRIG", "EE"),
		viron.NewCommandLine("STANDBY"),
	}
	for _, cmd := range cmds {
		conn.SetReadDeadline(time.Now().Add(time.Second * 5))
		_, err := dev.Exec(cmd)
		if err != nil {
			return err
		}
	}

	return nil
}

func vironStop(conn net.Conn, dev *viron.Device) error {
	conn.SetReadDeadline(time.Now().Add(time.Second * 5))
	_, err := dev.Exec(viron.NewCommandLine("STOP"))
	return err
}

func vironWait(ctx context.Context, timeout time.Duration, conn net.Conn,
	dev *viron.Device) error {
	conn.SetReadDeadline(time.Now().Add(timeout))

	s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
	s.Prefix = "Waiting for laser warm-up ... "
	s.FinalMSG = "\n"

	ch := make(chan error, 1)
	go func() { ch <- dev.WaitForReady(ctx, nil) }()
	s.Start()
	err := <-ch
	s.Stop()

	if err != nil {
		return err
	}

	conn.SetReadDeadline(time.Now().Add(time.Second * 5))
	_, err = dev.Exec(viron.NewCommandLine("FIRE"))

	return err
}

func imageSetup(client *imagesvc.Client, cfg iccdConfig) error {
	opts := []imagesvc.AcqOption{
		imagesvc.Trigger(imagesvc.HardwareTrigger),
		imagesvc.Exposure(time.Duration(cfg.Exposure)),
		imagesvc.Gain(cfg.Gain),
		imagesvc.Timeout(time.Second * 10),
	}
	return client.StartAcq(context.Background(), opts...)
}

func genFilename(t time.Time) string {
	s := t.Format("20060102_150405")
	return fmt.Sprintf("raman_%s_%03d.tiff", s, t.Nanosecond()/1000000)
}

func acquireImages(ctx context.Context, client *imagesvc.Client,
	count int, imgdir string) error {
	ctx = metadata.AppendToOutgoingContext(ctx, "image-chunksize",
		fmt.Sprintf("%d", 64*1024))
	for i := 0; i < count; i++ {
		rdr, err := client.Acquire(ctx)
		if err != nil {
			return fmt.Errorf("Cannot acquire image %d: %w", i, err)
		}
		name := genFilename(time.Now())
		file, err := os.Create(filepath.Join(imgdir, name))
		if err != nil {
			return fmt.Errorf("Create file %s: %w", name, err)
		}
		fmt.Printf("Saving image %q ... ", name)
		size, err := io.Copy(file, rdr)
		fmt.Printf("%d bytes\n", size)
		file.Close()
	}
	return nil
}

func grpcClient(address string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		return nil, err
	}

	return conn, err
}

func systemInit() system {
	var sys system

	p, err := serial.OpenPort(&serial.Config{
		Name:        intensDev,
		Baud:        intensBaud,
		ReadTimeout: time.Second * 5})
	if err != nil {
		log.Fatalf("Cannot open serial device %q: %v", intensDev, err)
	}
	sys.intensifier = iccd.NewIntensifier(iccd.NewBus(p, haveEcho), intensAddr)

	p, err = serial.OpenPort(&serial.Config{
		Name:        expDev,
		Baud:        expBaud,
		ReadTimeout: time.Second * 5})
	if err != nil {
		log.Fatalf("Cannot open serial device %q: %v", expDev, err)
	}
	sys.beDev = beamexp.New(p)

	sys.laserConn, err = net.Dial("tcp", laserAddr)
	if err != nil {
		log.Fatalf("Cannot access Viron laser at %q: %v", laserAddr, err)
	}
	sys.laserDev = viron.NewDevice(sys.laserConn)
	if err = sys.laserDev.StartSession(laserPword); err != nil {
		log.Fatalf("Cannot log-in to Viron laser: %v", err)
	}

	conn, err := grpcClient(svcAddr)
	if err != nil {
		log.Fatalf("Cannot access Image Server: %v", err)
	}
	sys.iClient = imagesvc.NewClient(conn)

	return sys
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&svcAddr, "server", lookupEnvOrString("IA_SERVER", svcAddr),
		"host:port for image acquisition server")
	flag.StringVar(&outDir, "image-dir", lookupEnvOrString("IMAGE_DIR", outDir),
		"image storage directory")
	flag.StringVar(&intensDev, "intens-dev",
		lookupEnvOrString("INTENS_DEVICE", intensDev),
		"image intensifier serial device")
	flag.IntVar(&intensBaud, "intens-baud",
		lookupEnvOrInt("INTENS_BAUD", intensBaud),
		"intensifier baud rate")
	flag.IntVar(&intensAddr, "intens-addr",
		lookupEnvOrInt("INTENS_ADDR", intensAddr),
		"intensifier rs-485 bus address")
	flag.BoolVar(&haveEcho, "has-echo", haveEcho,
		"true if the rs-485 interface has an echo")
	flag.StringVar(&expDev, "exp-dev",
		lookupEnvOrString("EXP_DEVICE", expDev),
		"beam expander serial device")
	flag.IntVar(&expBaud, "exp-baud",
		lookupEnvOrInt("EXP_BAUD", expBaud),
		"beam expander baud rate")
	flag.StringVar(&laserAddr, "laser-addr",
		lookupEnvOrString("VIRON_ADDR", laserAddr),
		"Viron laser host:port")
	flag.StringVar(&laserPword, "laser-pword",
		lookupEnvOrString("VIRON_PASSWORD", laserPword),
		"Viron laser password")
	flag.DurationVar(&laserWarmup, "laser-warmup",
		lookupEnvOrDuration("VIRON_WARMUP", laserWarmup),
		"Viron laser warmup time")
	flag.StringVar(&tbAddr, "tb-sock",
		lookupEnvOrString("TB_SOCKET", tbAddr),
		"Timing Board server socket")

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var err error
	imageCount := int(1)
	if len(args) > 1 {
		imageCount, err = strconv.Atoi(args[1])
		if err != nil {
			log.Fatalf("Bad image count, %q: %v", args[1], err)
		}
	}

	// Initialize all of the subsystems
	sys := systemInit()

	// Load the parameter file
	cfg, err := loadConfig(args[0])
	if err != nil {
		log.Fatal(err)
	}

	// Load the timing board parameters
	FpgaLoad.Data, _ = json.Marshal(cfg.Timing)
	err = sendMessages(tbAddr, FpgaDisable, FpgaLoad)
	if err != nil {
		log.Fatalf("Cannot send Timing Board message: %v", err)
	}

	// Setup the Viron laser
	err = vironSetup(sys.laserConn, sys.laserDev)
	if err != nil {
		log.Fatalf("Laser setup failed: %v", err)
	}

	// Send the image acquisition parameters to the RPC server
	err = imageSetup(sys.iClient, cfg.Iccd)
	if err != nil {
		log.Fatalf("CCD setup failed: %v", err)
	}

	// Set beam expander divergence value
	if cfg.Exp.Setting < 0 || cfg.Exp.Setting > 3200 {
		log.Fatalf("Invalid beam expander setting, %d", cfg.Exp.Setting)
	}
	err = sys.beDev.SetOffset(cfg.Exp.Setting)
	if err != nil {
		log.Fatalf("Cannot set beam expander value: %v", err)
	}

	// Configure the image intensifier to use an external trigger
	// and external gate
	err = sys.intensifier.SetGate(iccd.Gate{Type: iccd.ExternalGate})
	if err != nil {
		log.Fatalf("Cannot set ICCD gate: %v", err)
	}
	err = sys.intensifier.SetSync(iccd.ExternalSync)
	if err != nil {
		log.Fatalf("Cannot set ICCD trigger: %v", err)
	}
	// Set the intensifier gain
	err = sys.intensifier.SetGain(cfg.Iccd.Igain)
	if err != nil {
		log.Fatalf("Cannot set ICCD gain: %v", err)
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	// Wait for laser to warmup
	err = vironWait(ctx, laserWarmup, sys.laserConn, sys.laserDev)
	if err != nil {
		log.Fatal(err)
	}

	// Enable FPGA trigger output
	err = sendMessages(tbAddr, FpgaEnable)
	if err != nil {
		log.Print(err)
	}

	// Acquire images
	err = acquireImages(ctx, sys.iClient, imageCount, outDir)
	if err != nil {
		log.Print(err)
	}

	// Disable FPGA trigger output
	err = sendMessages(tbAddr, FpgaDisable)
	if err != nil {
		log.Print(err)
	}

	// Disable laser
	vironStop(sys.laserConn, sys.laserDev)
}
