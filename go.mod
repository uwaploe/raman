module bitbucket.org/uwaploe/raman

go 1.13

require (
	bitbucket.org/uwaploe/beamexp v0.3.0
	bitbucket.org/uwaploe/iccdctl v0.9.0
	bitbucket.org/uwaploe/imagesvc v0.7.0
	bitbucket.org/uwaploe/tbctl v0.5.2
	bitbucket.org/uwaploe/viron v0.9.0
	github.com/BurntSushi/toml v0.3.1
	github.com/briandowns/spinner v1.9.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	google.golang.org/grpc v1.27.0
)

replace bitbucket.org/uwaploe/imagesvc => ../pkg/imagesvc

replace bitbucket.org/uwaploe/tbctl => ../tbctl
