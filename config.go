package main

import (
	"fmt"
	"io/ioutil"

	"bitbucket.org/uwaploe/tbctl/pkg/tb"
	"github.com/BurntSushi/toml"
)

type expConfig struct {
	Setting int `toml:"setting"`
}

type iccdConfig struct {
	Igain    float32     `toml:"igain"`
	Gain     float32     `toml:"gain"`
	Exposure tb.Duration `toml:"exposure"`
}

type focusConfig struct {
	Setting int `toml:"setting"`
}

type sysConfig struct {
	Timing tb.Timing   `toml:"timing"`
	Exp    expConfig   `toml:"exp"`
	Iccd   iccdConfig  `toml:"iccd"`
	Focus  focusConfig `toml:"focus"`
}

func loadConfig(filename string) (sysConfig, error) {
	var cfg sysConfig

	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return cfg, fmt.Errorf("read parameter file: %w", err)
	}

	_, err = toml.Decode(string(contents), &cfg)

	return cfg, err
}
